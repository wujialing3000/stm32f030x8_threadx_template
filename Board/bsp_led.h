/**
 ******************************************************************************
 * @file    bsp_led.h
 * @author  Iron
 * @date    2020-01-01
 * @version v1.0
 * @brief   bsp_led header file
 */

#ifndef __BSP_LED_H
#define __BSP_LED_H

#ifdef __cplusplus
extern "C" {
#endif

/* Exported includes ---------------------------------------------------------*/
#include "bsp.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
#define BSP_LED_OFF     0
#define BSP_LED_ON      1

#define BSP_LED_BLINK_FOREVER   0xFFFFFFFF

/* Exported macro ------------------------------------------------------------*/
/* Exported functions prototypes ---------------------------------------------*/
int32_t bsp_led_init(void);
int32_t bsp_led_on(int32_t led_id);
int32_t bsp_led_off(int32_t led_id);
int32_t bsp_led_bink(int32_t led_id, uint8_t state, uint8_t blink_on, uint8_t blink_off, uint32_t blink_count);

/**
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif /* __BSP_LED_H */

/******************* (C)COPYRIGHT 2020 ***** END OF FILE *********************/
