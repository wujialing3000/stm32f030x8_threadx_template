/**
 ******************************************************************************
 * @file    bsp.h
 * @author  Iron
 * @date    2020-01-01
 * @version v1.0
 * @brief   bsp header file
 */

#ifndef __BSP_H
#define __BSP_H

#ifdef __cplusplus
extern "C" {
#endif

/* Exported includes ---------------------------------------------------------*/
#include <stdbool.h>
#include <stdint.h>
#include "stm32f0xx_hal.h"
#include "tx_api.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

void SystemClock_Config(void);
int32_t bsp_init(void);

/**
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif /* __BSP_H */

/******************* (C)COPYRIGHT 2020 ***** END OF FILE *********************/
