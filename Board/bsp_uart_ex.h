/**
  ******************************************************************************
  * @file    bsp_uart_ex.h 
  * @author  Iron
  * @date    2021-01-31
  * @version v1.0
  * @brief   bsp_uart_ex header file
  */

#ifndef __BSP_UART_EX_H
#define __BSP_UART_EX_H

#ifdef __cplusplus
 extern "C" {
#endif 

/* Exported includes ---------------------------------------------------------*/
#include "bsp_uart.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions prototypes ---------------------------------------------*/
int32_t bsp_uart_poll_tx(bsp_uart_id_t uart_id, uint8_t *buf, size_t buflen, uint32_t timeout);
int32_t bsp_uart_poll_rx(bsp_uart_id_t uart_id, uint8_t *buf, size_t buflen, uint32_t timeout);

int32_t bsp_uart_it_tx(bsp_uart_id_t uart_id, uint8_t *buf, size_t buflen, uint32_t timeout);
int32_t bsp_uart_it_rx(bsp_uart_id_t uart_id, uint8_t *buf, size_t buflen, uint32_t timeout);

int32_t bsp_uart_dma_tx(bsp_uart_id_t uart_id, uint8_t *buf, size_t buflen, uint32_t timeout);
int32_t bsp_uart_dma_rx(bsp_uart_id_t uart_id, uint8_t *buf, size_t buflen, uint32_t timeout);

int32_t bsp_usart_init_ex(void);

/**
  * @}
  */

#ifdef __cplusplus
}
#endif

#endif /* __BSP_UART_EX_H */

/******************* (C)COPYRIGHT 2021 ***** END OF FILE *********************/
