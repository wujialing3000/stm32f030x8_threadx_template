/**
  ******************************************************************************
  * @file    bsp_flash_iap.h 
  * @author  Iron
  * @date    2021-02-07
  * @version v1.0
  * @brief   bsp_flash_iap header file
  */

#ifndef __BSP_FLASH_IAP_H
#define __BSP_FLASH_IAP_H

#ifdef __cplusplus
 extern "C" {
#endif 

/* Exported includes ---------------------------------------------------------*/
#include "bsp.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions prototypes ---------------------------------------------*/
   
int bsp_flash_iap_read(uint32_t addr, uint8_t *data, size_t size);

int bsp_flash_iap_write(uint32_t addr, const uint8_t *data, size_t size);

int bsp_flash_iap_erase(uint32_t addr, size_t size);


/**
  * @}
  */

#ifdef __cplusplus
}
#endif

#endif /* __BSP_FLASH_IAP_H */

/******************* (C)COPYRIGHT 2021 ***** END OF FILE *********************/
