/**
  ******************************************************************************
  * @file    template.h 
  * @author  Iron
  * @date    2021-01-01
  * @version v1.0
  * @brief   template header file
  */

#ifndef __TEMPLATE_H
#define __TEMPLATE_H

#ifdef __cplusplus
 extern "C" {
#endif 

/* Exported includes ---------------------------------------------------------*/
/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions prototypes ---------------------------------------------*/
void bsp_watchdog_feed(void);


/**
  * @}
  */

#ifdef __cplusplus
}
#endif

#endif /* __TEMPLATE_H */

/******************* (C)COPYRIGHT 2021 ***** END OF FILE *********************/
