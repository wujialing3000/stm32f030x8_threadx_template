/**
 ******************************************************************************
 * @file    bsp_uart.h
 * @author  Iron
 * @date    2020-01-01
 * @version v1.0
 * @brief   bsp_usart header file
 */

#ifndef __BSP_USART_H
#define __BSP_USART_H

#ifdef __cplusplus
extern "C" {
#endif

/* Exported includes ---------------------------------------------------------*/
#include "bsp.h"

/* Exported types ------------------------------------------------------------*/
typedef enum {BSP_UART_NULL, BSP_UART1, BSP_UART2} bsp_uart_id_t;

/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions prototypes ---------------------------------------------*/
UART_HandleTypeDef *bsp_get_uart_handle(bsp_uart_id_t uart_id);
bsp_uart_id_t bsp_get_uart_id(UART_HandleTypeDef *huart);
int32_t bsp_usart_init(void);


/**
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif /* __BSP_USART_H */

/******************* (C)COPYRIGHT 2020 ***** END OF FILE *********************/
