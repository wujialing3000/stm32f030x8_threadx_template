# STM32F030x8_Threadx_Template

#### Description
STM32F030X8 Flash 64K SRAM 8K ThreadX porting application test, with the preparation of a serial port terminal Shell, used to assist the development test.


Serial Shell supports cursor movement and insert editing, caching 5 recent history by default, and supports table key matching commands.

Flash takes 20K to 35K

The remaining available RAM space is 3.7K


! [Threadx Shell](Doc/threadx.gif)



MDK 5.30 AC6:

- Debug Program Size: Code=35726 RO-data=1954 RW-data=44 ZI-data=3284

- Release Program Size: Code=20690 RO-data=906 RW-data=28 ZI-data=3180


IAR ARM 8.50.6 compilation information


Debug:

- 28'538 bytes of readonly code memory

- 2'224 bytes of readonly data memory

- 3'789 bytes of readwrite data memory


Release:

- 23'080 bytes of readonly code memory

- 959 bytes of readonly data memory

- 3'728 bytes of readwrite data memory