/**
  ******************************************************************************
  * @file    shell-serial.c
  * @author  Iron
  * @date    2021-01-29
  * @version v1.0
  * @brief   shell-serial c file
  */

/** @addtogroup GROUP_SHELL
  * @{
  */

/* Private includes ----------------------------------------------------------*/
#include <stdio.h>
#include <stdarg.h>
#include "bsp_uart_ex.h"
#include "shell-serial.h"
/* tx os api */
#include "tx_api.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define TX_SHELL_STACK_SIZE         512
#define TX_SHELL_PRIORITY           TX_USER_THREAD_PRIORITY_BELOW_NORMAL
#define TX_SHELL_PREEMPT_THRESHOLD  TX_SHELL_PRIORITY

/*---------------------------------------------------------------------------*/
#define SHELL_BUF_SIZE              100
#define SHELL_UART                  BSP_UART2

/*---------------------------------------------------------------------------*/
//#define CMD_TAG             "\r\n>"
#define NEW_LINE            "\r\n"

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

/* serial helle omessage */
static const char *wellcom =
    NEW_LINE
    "ThreadX-based Serial Terminal Shell" NEW_LINE
    "Copyright (c) 2021, Author: xwiron, Email: xwiron@aliyun.com" NEW_LINE
    "Type \"help\" to view a list of registered commands" NEW_LINE
    NEW_LINE;

/* serial shell context */
static shell_context_t shell_serial_context;
struct shell_command_sets_t shell_serial_command_sets;
struct shell_history_queue_t shell_serial_history;

/* Private function prototypes -----------------------------------------------*/

static void *shell_malloc(size_t size)
{
    return tx_user_malloc(size);
}

static void shell_free(void *ptr)
{
    tx_user_free(ptr);
}

/*----------------------------------------------------------------------------*/
static int shell_serial_printf(const char *fmt, ...)
{
    va_list args;
    int32_t len;
    char *buf;

    buf = shell_malloc(SHELL_BUF_SIZE + 1);

    if (buf == NULL)
        return 0;

    va_start(args, fmt);
    len = vsnprintf(buf, SHELL_BUF_SIZE, fmt, args);
    va_end(args);

    buf[len] = '\0';

    bsp_uart_poll_tx(SHELL_UART, (uint8_t *)buf, len, TX_WAIT_FOREVER);
//    bsp_uart_dma_tx(SHELL_UART, (uint8_t *)buf, len, TX_WAIT_FOREVER);

    shell_free(buf);

    return len;
}


/*----------------------------------------------------------------------------*/
static int shell_serial_puts(const char *str)
{
    int len;

    len = strlen(str);

    bsp_uart_poll_tx(SHELL_UART, (uint8_t *)str, len, TX_WAIT_FOREVER);

    return len;
}

/*----------------------------------------------------------------------------*/
static int shell_serial_putchar(char ch)
{
    bsp_uart_poll_tx(SHELL_UART, (uint8_t *)&ch, 1, TX_WAIT_FOREVER);

    return ch;
}

/*----------------------------------------------------------------------------*/
static int shell_serial_getchar(void)
{
    char data = EOF;

    bsp_uart_it_rx(SHELL_UART, (uint8_t *)&data, 1, TX_WAIT_FOREVER);

    return data;
}

/*---------------------------------------------------------------------------*/
void shell_serial(struct shell_command_set_t *set)
{
    shell_context_t *shell = &shell_serial_context;

    shell->name = "shell-serial";

    shell->shell_getchar = shell_serial_getchar;
    shell->shell_putchar = shell_serial_putchar;
    shell->shell_puts = shell_serial_puts;
    shell->shell_printf = shell_serial_printf;
    shell->shell_malloc = shell_malloc;
    shell->shell_free = shell_free;

    shell_init(shell, &shell_serial_command_sets, &shell_serial_history);

    if (set)
    {
        shell_command_set_register(shell->command_sets, set);
    }

    shell_serial_puts(SHELL_END_OF_LINE "Shell init done, Press any key to continue..." SHELL_END_OF_LINE);

    shell_serial_getchar();
    shell_serial_puts(wellcom);
    shell_output_prompt(shell);

    for (;;)
    {
        int data = shell_serial_getchar();

        if (data != EOF)
        {
            char *buf = shell_malloc(SHELL_BUF_SIZE + 1); // +1 '\0'

            if (buf != NULL)
            {
                buf[0] = data;
                buf[1] = '\0';

                shell_getline(shell, buf, SHELL_BUF_SIZE);

                shell_input(shell, buf);

                shell_free(buf);
            }
            else
            {
                /* shell malloc error */
                shell_serial_printf("Shell malloc menmory error.");
            }
        }

    }
}

/**
  * @}
  */

/******************* (C)COPYRIGHT 2021 ***** END OF FILE *********************/
