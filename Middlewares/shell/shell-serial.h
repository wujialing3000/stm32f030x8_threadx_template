/**
  ******************************************************************************
  * @file    shell-serial.h
  * @author  Iron
  * @date    2021-01-29
  * @version v1.0
  * @brief   shell-serial header file
  */

#ifndef __SHELL_SERIAL_H
#define __SHELL_SERIAL_H

#ifdef __cplusplus
extern "C" {
#endif

/* Exported includes ---------------------------------------------------------*/
#include "shell.h"
#include "shell-commands.h"
#include "shell-history.h"


/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions --------------------------------------------------------*/
void shell_serial(struct shell_command_set_t *set);

/**
  * @}
  */

#ifdef __cplusplus
}
#endif

#endif /* __SHELL_SERIAL_H */

/******************* (C)COPYRIGHT 2021 ***** END OF FILE *********************/
