/*
 * Copyright (c) 2020, Armink, <armink.ztl@gmail.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef _FAL_CFG_H_
#define _FAL_CFG_H_

/* FlashDB config file */
#include <fdb_cfg.h>

/* flash tabel config */
#define FAL_PART_HAS_TABLE_CFG

/* ===================== Flash device Configuration ========================= */
#define FAL_PART_TABLE_FLASH_DEV_NAME   "stm32_onchip"
#define FAL_PART_TABLE_END_OFFSET       0

extern const struct fal_flash_dev stm32_onchip_flash;

/* flash device table */
#define FAL_FLASH_DEV_TABLE                                          \
{                                                                    \
    &stm32_onchip_flash,                                             \
}

/* ====================== Partition Configuration =========================== */
#ifdef FAL_PART_HAS_TABLE_CFG

#ifdef FDB_USING_KVDB
    #define FDB_KVDB_CFG  {FAL_PART_MAGIC_WORD,  "fdb_kvdb1",    FAL_PART_TABLE_FLASH_DEV_NAME,   56*1024, 8*1024, 0},
#else
    #define FDB_KVDB_CFG
#endif

#ifdef FDB_USING_TSDB
    #define FDB_TSDB_CFG  {FAL_PART_MAGIC_WORD,  "fdb_tsdb1",    FAL_PART_TABLE_FLASH_DEV_NAME,   48*1024,  8*1024, 0},
#else
    #define FDB_TSDB_CFG
#endif

/* partition table */
#define FAL_PART_TABLE      \
{                           \
    FDB_KVDB_CFG           \
    FDB_TSDB_CFG           \
}
#else

#endif /* FAL_PART_HAS_TABLE_CFG */

/* ======================= Menmory Configuration ============================ */
/* memory function */
#include "tx_api.h"

#define FAL_MALLOC  tx_user_malloc
#define FAL_CALLOC  tx_user_calloc
#define FAL_REALLOC tx_user_realloc
#define FAL_FREE    tx_user_free

/* ======================== Debug Configuration ============================= */
#ifndef NDEBUG
    #define FAL_DEBUG 1
    #define FAL_PRINTF(...) tx_user_printf(__VA_ARGS__)
#else
    #define FAL_DEBUG 0
    #define FAL_PRINTF(...)
#endif

#endif /* _FAL_CFG_H_ */
