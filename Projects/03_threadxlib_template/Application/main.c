/**
 ******************************************************************************
 * @file    main.c
 * @author  Iron
 * @date    2020-01-01
 * @version v1.0
 * @brief   main c file
 */

/* Private includes ----------------------------------------------------------*/
#include "tx_api.h"
#include "bsp.h"

/* BEBUG LOG */
#include "debug_log.h"
#define LOG_LOCAL_LEVEL     DBG_LOG_DEBUG
DBG_LOG_TAG("MAIN");

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/
int main(void)
{
    /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
    HAL_Init();

    /* Configure the system clock */
    SystemClock_Config();

    /* Enter the ThreadX kernel.  */
    tx_kernel_enter();

    return 0;
}

/**
 * @brief  This function is executed in case of error occurrence.
 * @retval None
 */
void Error_Handler(void)
{
    /* USER CODE BEGIN Error_Handler_Debug */
    /* User can add his own implementation to report the HAL error return state */
    DBG_LOGE(TAG, "system fatal error.");

    __disable_irq();
    while(1);
    /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
    /* USER CODE BEGIN 6 */
    /* User can add his own implementation to report the file name and line number,
       ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
    /* USER CODE END 6 */

    DBG_LOGE(TAG, "assert_failed at '%s':%d.", file, line);

    Error_Handler();
}
#endif /* USE_FULL_ASSERT */

/**
 * @}
 */

/******************* (C)COPYRIGHT 2020 ***** END OF FILE *********************/
